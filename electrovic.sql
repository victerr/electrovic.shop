-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 15-12-2019 a las 19:40:12
-- Versión del servidor: 5.7.24
-- Versión de PHP: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `electrovic`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulo`
--

CREATE TABLE `articulo` (
  `id` int(11) NOT NULL,
  `nombre` varchar(200) CHARACTER SET latin1 NOT NULL,
  `descripcion` varchar(500) CHARACTER SET latin1 NOT NULL,
  `precio` int(11) NOT NULL,
  `categoria` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `estado` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `foto` varchar(255) CHARACTER SET latin1 NOT NULL,
  `fecha` datetime DEFAULT CURRENT_TIMESTAMP,
  `usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `articulo`
--

INSERT INTO `articulo` (`id`, `nombre`, `descripcion`, `precio`, `categoria`, `estado`, `foto`, `fecha`, `usuario`) VALUES
(89, 'Patata', 'Sí', 55, 'Tubérculos', 'Con tierra', 'uploads/KUzrF0Bw.png', '2019-12-14 21:33:35', 11),
(90, 'Patata', 'Una patata preciosa.', 55, 'Tubérculos', 'Con tierra', 'uploads/60ceca2355a3eba375450ea41cc239b0.png', '2019-12-14 22:21:43', 11);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mensajes`
--

CREATE TABLE `mensajes` (
  `id` int(11) NOT NULL,
  `remitente` int(11) NOT NULL,
  `destinatario` int(11) NOT NULL,
  `texto` varchar(800) COLLATE utf8_spanish2_ci NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `avatar` varchar(255) NOT NULL,
  `fecha_nac` date DEFAULT NULL,
  `rango` varchar(30) NOT NULL,
  `fecha_registro` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `role` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `username`, `email`, `password`, `avatar`, `fecha_nac`, `rango`, `fecha_registro`, `role`) VALUES
(1, 'victor2', 'placeholder', 'victor2', '', '2019-12-15', 'NOOB', '2019-12-14 08:00:00', 'ROLE_ADMIN'),
(11, 'victor', 'ivorra.alberola@gmail.com', '$2y$10$pYmbC9iHLF9T0kTV2qgj6ukNeMZcS5NAGpaM/VVYMYTztBFeRsOaq', '', '2019-12-15', 'NOOB', '2019-12-14 08:00:00', 'ROLE_ADMIN'),
(12, 'admin', 'admin@electrovic.com', '$2y$10$1jm/iQJeYxZP7freTBjVxeB54jOoNIN1Utj82qWgEAdZVXQSrzkZu', '', '2019-12-15', 'NOOB', '2019-12-14 08:00:00', 'ROLE_ADMIN'),
(13, 'victor3', 'victor3@gmail.com', '$2y$10$oJ6ZS/x2qbFILVIPYwFi3Oz4GqrE0J8eCi9GYHEWginYzA49qAq.O', '', NULL, 'NOOB', '2019-12-15 12:07:37', 'ROLE_USER'),
(14, 'Haru', 'haruhiro@gmail.com', '$2y$10$GhBHifHvEGYTkhC.Mx75MOq660gOHB8cP1QvBp1gveWwZzIHfRZfe', '', NULL, 'NOOB', '2019-12-15 14:02:26', 'ROLE_USER'),
(15, 'haruhiro', 'haruhiro2@gmail.com', '$2y$10$OJQnFkwK2AcoRr8c0CGsl.Z1mBKVJFns6dJbl.COXJrhE9Rh0sQbS', '', NULL, 'NOOB', '2019-12-15 14:09:08', 'ROLE_USER'),
(16, 'victor4', 'victor4@gmail.com', '$2y$10$oIlz5ipITLW9I2ABk5HC5.OTtdsJ6/zWIiAX.uhBxgCo67CUKauru', '', NULL, 'NOOB', '2019-12-15 14:11:43', 'ROLE_USER'),
(17, 'victor5', 'victor5@gmail.com', '$2y$10$XsugD6ohnCI5.4CS385K4.f1Gcp6/ynWBx4PkD8dGu7er/JwI56IC', '', NULL, 'NOOB', '2019-12-15 14:14:07', 'ROLE_USER'),
(19, 'victor6', 'victor6@gmail.com', '$2y$10$THrZMOaJ8eG43uX.mFN2q.WDj1qt.kPBeUTLfu6zlcOOMN4OnTHaG', '', NULL, 'NOOB', '2019-12-15 14:14:37', 'ROLE_USER'),
(20, 'victor7', 'victor7@gmail.com', '$2y$10$l9JE.TCmjex9AmAPc2A7xO.e.4RfXx0dEY8LWPyEC5qjc3u.8WmPm', '', NULL, 'NOOB', '2019-12-15 14:15:30', 'ROLE_USER'),
(21, 'victor8', 'victor8@gmail.com', '$2y$10$tJ5aso9jnrbgOD23FFmqieTB0.FuxdHtrYvOIAPFZ6TUtOJyqM5XW', '', NULL, 'NOOB', '2019-12-15 14:19:22', 'ROLE_USER'),
(22, 'victor9', 'victor9@gmail.com', '$2y$10$km4BZRgqGeQaF3rjUnA06OqTYb7rY6PuIv43VEzN3bl0eB2q.gMHy', '', NULL, 'NOOB', '2019-12-15 14:19:41', 'ROLE_USER'),
(23, 'victor10', 'victor10@gmail.com', '$2y$10$XybxIOLn6r.eod7f3YexveFbKlNiHK6PHKjQUUNcWlumlsm9pbPCC', '', NULL, 'NOOB', '2019-12-15 14:20:49', 'ROLE_USER'),
(24, 'haruhiro4', 'haruhiro4@gmail.com', '$2y$10$V04wsLNV6JovYgAuTRJd5OD6SgSbmkmrzXFFbW6abN8bIEgiqcyP2', 'uploads/avatares/Haru4.png', NULL, 'NOOB', '2019-12-15 14:28:42', 'ROLE_USER');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `articulo`
--
ALTER TABLE `articulo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mensajes`
--
ALTER TABLE `mensajes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mensaje_remitente` (`remitente`),
  ADD KEY `mensaje_destinatario` (`destinatario`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `articulo`
--
ALTER TABLE `articulo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT de la tabla `mensajes`
--
ALTER TABLE `mensajes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `mensajes`
--
ALTER TABLE `mensajes`
  ADD CONSTRAINT `mensaje_destinatario` FOREIGN KEY (`destinatario`) REFERENCES `usuario` (`id`),
  ADD CONSTRAINT `mensaje_remitente` FOREIGN KEY (`remitente`) REFERENCES `usuario` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
