<?php

$router->get('', 'ArticuloController@listar');

$router->get('articulos', 'ArticuloController@listar');
$router->post('articulos', 'ArticuloController@buscar'); //No le pongo rango para que se vean cuando no estás logeado.
$router->get('articulos/nuevo', 'ArticuloController@nuevoArticulo', 'ROLE_USER');
$router->post('articulos/enviar', 'ArticuloController@nuevo', 'ROLE_USER');
$router->get('articulos/categoria/:categoria', 'ArticuloController@listar');
$router->get('articulos/:id', 'ArticuloController@show'); //No le pongo rango para que se vean cuando no estás logeado
$router->delete('articulos/:id', 'ArticuloController@eliminar', 'ROLE_USER');
$router->get('articulos/:id/editar', 'ArticuloController@editar', 'ROLE_USER');
$router->get('articulos/:id/foto', 'ArticuloController@getFoto'); //No le pongo rango para que se vean cuando no estás logeado.
$router->get('articulos/:id/miniatura', 'ArticuloController@getMiniatura');//No le pongo rango para que se vean cuando no estás logeado.
$router->post('articulos/:id/actualizar', 'ArticuloController@actualizar', 'ROLE_USER');

$router->get('login', 'AuthController@login');
$router->post('check-login', 'AuthController@checkLogin');
$router->get('logout', 'AuthController@logout', 'ROLE_USER');

$router->get('registro', 'PagesController@registro');
$router->get('carro', 'PagesController@carro', 'ROLE_USER');

$router->get('usuarios', 'UsuarioController@listar', 'ROLE_ADMIN');
$router->post('usuarios/nuevo', 'UsuarioController@nuevo');
$router->get('usuarios/:id', 'UsuarioController@verPerfil', 'ROLE_USER');
$router->delete('usuarios/:id', 'UsuarioController@eliminar', 'ROLE_ADMIN');
$router->get('usuarios/carro/:id', 'UsuarioController@carro', 'ROLE_USER');
$router->delete('usuarios/carro/:id', 'UsuarioController@eliminarDelCarro', 'ROLE_USER');
$router->get('usuarios/:id/editar', 'UsuarioController@editar', 'ROLE_USER');
$router->get('usuarios/:id/articulos', 'UsuarioController@verArticulos', 'ROLE_USER');
$router->get('usuarios/:id/foto', 'UsuarioController@getFoto'); //No le pongo rango para que se vean cuando no estás logeado.
$router->get('usuarios/:id/miniatura', 'UsuarioController@getMiniatura');
$router->post('usuarios/:id/actualizaradmin', 'UsuarioController@actualizarAdmin', 'ROLE_ADMIN');
$router->post('usuarios/:id/actualizar', 'UsuarioController@actualizar', 'ROLE_USER');

$router->get('mensajes', 'MensajeController@listar', 'ROLE_USER');
$router->get('mensajes/conversacion/:id', 'MensajeController@verConversacion', 'ROLE_USER');
$router->post('mensajes/enviar/:id', 'MensajeController@enviar', 'ROLE_USER');