<?php
namespace EV\app\controllers;


use EV\app\entity\Mensaje;
use EV\app\repository\Mensaje_EnviadoRepository;
use EV\app\repository\Mensaje_RecibidoRepository;
use EV\app\repository\UsuarioRepository;
use EV\core\App;
use EV\core\exceptions\MyException;
use EV\core\Response;

class MensajeController
{
    /**
     * @return MensajeRecibidoRepository
     */
    private function getMensaje_RecibidoRepository() : Mensaje_RecibidoRepository
    {
        return App::getRepository(Mensaje_RecibidoRepository::class);
    }
    private function getMensaje_EnviadoRepository() : Mensaje_EnviadoRepository
    {
        return App::getRepository(Mensaje_EnviadoRepository::class);
    }

    public function getConversaciones(){
        $conversaciones = [[]];
        $usuario=App::get('usuario');
        $mensajesRecibidos = $this->getMensaje_RecibidoRepository()->findBy(['destinatario' => $usuario->getId()]);
        $mensajesEnviados = $this->getMensaje_EnviadoRepository()->findBy(['remitente' => $usuario->getId()]);

        foreach ($mensajesRecibidos as $mensaje) {
            foreach ($conversaciones as $key => $conversacion){
                if($conversacion==null){
                    $conversaciones[$mensaje->getRemitente()][$mensaje->getId()]=$mensaje;
                }
                else
                    if($conversaciones[$mensaje->getRemitente()][$mensaje->getId()]->getRemitente()==$mensaje->getRemitente()){
                        $conversaciones[$mensaje->getRemitente()][$mensaje->getId()]=$mensaje;
                    }
                    else
                        $conversaciones[$mensaje->getRemitente()][$mensaje->getId()]=$mensaje;
            }
        }

        foreach ($mensajesEnviados as $mensaje) {
            foreach ($conversaciones as $key => $conversacion){
                if($conversacion==null){
                    $conversaciones[$mensaje->getDestinatario()][$mensaje->getId()]=$mensaje;
                }
                else
                    if($conversaciones[$mensaje->getDestinatario()][$mensaje->getId()]->getDestinatario()==$mensaje->getDestinatario()){
                        $conversaciones[$mensaje->getDestinatario()][$mensaje->getId()]=$mensaje;
                    }
                    else
                        $conversaciones[$mensaje->getDestinatario()][$mensaje->getId()]=$mensaje;
            }
        }

        return $conversaciones;
    }

    public function listar()
    {
        $conversaciones=$this->getConversaciones();
        $usuarioRepositorio = UsuarioRepository::getRepository();

        $conveFiltradas = [];
        foreach ($conversaciones as $conversacion){
            array_push($conveFiltradas,end($conversacion));
        }
        
        //TODO: Mostrar conversaciones. Paso el último mensaje a la vista, el resto de la conversación se cargará cuando se seleccione una

        Response::renderView('mensajes', [
            'conversaciones' => $conveFiltradas,
            'usuarioRepositorio' => $usuarioRepositorio
        ]);
    }

    public function verConversacion(int $id)
    {
        $conversacion=$this->getConversaciones()[$id];

        $usuarioRepository = App::getRepository(UsuarioRepository::class);
        /*$remitenteId = end($conversacion)->getRemitente();*/

        Response::renderView('conversacion', ['conversacion' => $conversacion,
            'usuarioRepository' => $usuarioRepository,
            'remitenteId' => $id]);
    }

    public function enviar(int $id)
    {
        $texto = $_POST['mensaje'];
        $usuarioId=App::get('usuario')->getId();

        $msg = new Mensaje();
        $msg->setMensaje($texto);
        $msg->setDestinatario($id);
        $msg->setRemitente($usuarioId);

        Mensaje_EnviadoRepository::getRepository()->save($msg);
        Mensaje_RecibidoRepository::getRepository()->save($msg);

        App::get('router')->redirect('mensajes/conversacion/'.$id);
    }
}