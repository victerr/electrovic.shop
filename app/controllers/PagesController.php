<?php

namespace EV\app\controllers;

use EV\app\repository\ArticuloRepository;
use EV\core\App;
use EV\core\helpers\FlashMessage;
use EV\core\Response;

class PagesController
{
    public function inicio()
    {
        $articuloRepository = ArticuloRepository::getRepository();
        $articulos = $articuloRepository->findAll();

        $mensaje = FlashMessage::get('mensaje');
        $error = FlashMessage::get('error');
        $numArticulos = count($articulos);

        Response::renderView('articulos', [
            'articulos' => $articulos,
            'articuloRepository' => $articuloRepository,
            'mensaje' => $mensaje,
            'error' => $error
        ]);
    }

    public function registro()
    {
        Response::renderView('registro');
    }

    private function error(string $message, string $errorMessage, int $errorCode)
    {
        header("HTTP/1.1 $errorCode $errorMessage", true, $errorCode);
        Response::renderView('error', [
            'message' => $message,
            'errorCode' => $errorCode,
            'errorMessage' => $errorMessage
        ]);
    }

    public function carro()
    {
        $carro = App::get('carro');
        $total=0;
        if(!is_null($carro))
            foreach ($carro as $articulo){
                $total=$total+$articulo->getPrecio();
            }

        Response::renderView('carro', [
            'articulos' => $carro,
            'total' => $total
        ]);
    }

    public function errorInterno(string $message)
    {
        $this->error($message, $errorMessage = 'Internal Server Error',  $errorCode = 500);
    }

    public function accesoDenegado(string $message)
    {
        $this->error($message, $errorMessage = 'Forbidden',  $errorCode = 403);
    }

    public function notFound(string $message)
    {
        $this->error($message, $errorMessage = 'Page not Found',  $errorCode = 404);
    }
}