<?php
namespace EV\app\controllers;


use EV\app\repository\Mensaje_EnviadoRepository;
use EV\core\App;
use EV\core\Response;

class Mensaje_EnviadoController extends MensajeController
{
    /**
     * @return Mensaje_EnviadoRepository
     */
    private function getMensaje_Enviado_EnviadoRepository() : Mensaje_Enviado_EnviadoRepository
    {
        return App::getRepository(Mensaje_Enviado_EnviadoRepository::class);
    }

}