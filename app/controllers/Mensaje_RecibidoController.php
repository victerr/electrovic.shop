<?php
namespace EV\app\controllers;


use EV\app\repository\MensajeRecibidoRepository;
use EV\core\App;
use EV\core\Response;

class MensajeRecibidoController extends MensajeController
{
    /**
     * @return MensajeRecibidoRepository
     */
    private function getMensajeRepository() : MensajeRecibidoRepository
    {
        return App::getRepository(MensajeRecibidoRepository::class);
    }

}