<?php

use Monolog\Logger;

return [
    'debug' => false,
    'database' => [
        'dbname' => 'electrovic',
        'dbuser' => 'electrovic',
        'dbpassword' => 'electrovic',
        'connection' => 'mysql:host=electrovic.com',
        'options' => [
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_PERSISTENT => true
        ]
    ],
    'logger' => [
        'filename' => __DIR__ . '/../logs/app.log',
        'log_level' => Logger::INFO
    ],
    'security' => [
        'roles' => [
            'ROLE_ADMIN'=>3,
            'ROLE_USER'=>2,
            'ROLE_ANONYMOUS'=>1
        ]
    ],
    'app' => [
        'rangos' => [
            'Noob',
            'Primeros pasos',
            'Vas mejorando',
            'Usuario avanzado',
            'Pro'
        ],
        'categorias' => [
            'Móviles',
            'Portátiles',
            'Sobremesa',
            'Consolas'
        ]
    ]
];