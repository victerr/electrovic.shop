<div class="row">
    <form method="post" action="/usuarios/nuevo" enctype="multipart/form-data">
        <div class="col col-md-5">
            <div class="form-group">
                <label for="username">Nombre de usuario</label>
                <input type="text" class="form-control" name="username">
            </div>

            <div class="form-group">
                <label for="password">Contraseña</label>
                <input type="password" class="form-control" name="password">
            </div>
            <div class="form-group col-md-6">
                <label for="avatar">Imagen</label>
                <input type="file" class="form-control-file" name="avatar">
            </div>

        </div>
        <div class="col col-md-5">
            <div class="form-group">
                <label for="email">Email: </label>
                <input type="email" class="form-control" name="email">
            </div>
            <div class="form-group">
                <label for="password2">Repite la contraseña:</label>
                <input type="password" class="form-control" name="password2">
                <br>
            </div>
        </div>
        <div>
            <button type="submit" class="btn btn-success col-md-5">Enviar</button>
        </div>
    </form>
</div>
<div class="row">
    <?php if (isset($id)) :?>
    <form action="/usuarios/<?= $id ?>/actualizaradmin" method="post">
        <?php endif; ?>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">Operaciones</th>
                <th scope="col">ID</th>
                <th scope="col">Nombre de usuario</th>
                <th scope="col">Email</th>
                <th scope="col">Rol</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($usuarios as $usuario ) : ?>
                <tr>
                    <?php if (isset($id) && $id == $usuario->getId()) :?>
                        <td>
                            <div class="btn-group" role="group" aria-label="Operaciones">
                                <button class="btn btn-secondary"><i class="fa fa-save"></i></button>
                                <a href="/usuarios" class="btn btn-secondary"><i class="fa fa-times"></i></a>
                            </div>
                        </td>
                        <td>
                            <input type="text" name="id-disabled" disabled value="<?= $usuario->getId() ?>">
                        </td>
                        <td><input type="text" name="username" value="<?= $usuario->getUsername() ?>"></td>
                        <td><input type="text" name="email" value="<?= $usuario->getEmail() ?>"></td>
                        <td>
                            <select name="rol">
                                <?php foreach ($roles as $rol => $value) : ?>
                                    <option value="<?= $rol ?>"><?=$rol?></option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    <?php else: ?>
                        <td>
                            <div class="btn-group" role="group" aria-label="Operaciones">
                                <a href="/usuarios/<?= $usuario->getId() ?>/editar" class="btn btn-secondary"><i class="fa fa-edit"></i></a>
                                <a href="/usuarios/<?= $usuario->getId() ?>" class="btn btn-secondary"><i class="fa fa-trash"></i></a>
                                <a href="/usuarios/<?= $usuario->getId() ?>" class="btn btn-secondary"><i class="fa fa-eye"></i></a>
                            </div>
                        </td>
                        <td><?= $usuario->getId() ?></td>
                        <td><?= $usuario->getUsername() ?></td>
                        <td><?= $usuario->getEmail() ?></td>
                        <td><?= $usuario->getRole() ?></td>
                    <?php endif; ?>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <?php if (isset($id)) :?>
    </form>
<?php endif; ?>
</div>
<script src="/js/usuarios.js"></script>
