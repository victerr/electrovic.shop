<br>
<div class="container">
    <?php foreach (array_reverse($articulos) as $articulo ) : ?>
        <e>
            <div class="card col-md-6 text-center" >
                <img src="/articulos/<?= $articulo->getId() ?>/foto" class="card-img-top" height="250px" alt="/articulos/<?= $articulo->getId() ?>/foto">
                <div class="card-body">
                    <div class="row">
                    <div class="col-md-3">
                        <h4 class="card-title"><?= $articulo->getNombre() ?></h4>
                    </div>
                    <div class="col-md-3 pull-right"><h5>Vendedor: <a href="usuarios/<?= $articulo->getUsuario() ?>"><?= ($usuarioRepository->find($articulo->getUsuario()))->getUsername() ?></a></h5>
                    </div>
                    </div>
                    <p class="card-text"><?= $articulo->getDescripcion() ?></p>

                    <div class="row">
                        <div class="col-md-3">
                            <h6>Precio:</h6><?= $articulo->getPrecio() ?> €
                        </div>
                        <div class="col-md-5">
                            <h6>Categoria:</h6><?= $articulo->getCategoria() ?>
                        </div>
                        <div class="col-md-4">
                            <h6>Estado:</h6><?= $articulo->getEstado() ?>
                        </div>
                    </div><br>
                    <a href="/articulos/<?= $articulo->getId() ?>" class="btn btn-primary">Ver</a>
                    <?php if ($_usuario) :?>
                        <div class="row">

                            <?php if ($_usuario->getId() == $articulo->getUsuario() || $_usuario->getRole()=='ROLE_ADMIN') :?>
                                <a href="/articulos/<?= $articulo->getId() ?>" class="btn btn-danger"><i class="patata">Eliminar</i></a>
                                <a href="/articulos/<?= $articulo->getId() ?>/editar" class="btn btn-primary">Modificar</a>
                            <?php endif; ?>
                        </div>

                        <?php if ($_usuario->getId() != $articulo->getUsuario()) :?>
                            <div class="row">
                                <a href="/usuarios/carro/<?= $articulo->getId() ?>" class="btn btn-primary">Al carro</a>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>
        </e>
    <?php endforeach; ?>
</div>
<script src="/js/articulos.js"></script>