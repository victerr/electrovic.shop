<br>

<table class="table table-hover">
    <thead class="thead-dark">
    <tr>
        <th scope="col">Usuario</th>
        <th scope="col">Mensaje</th>
        <th scope="col">Fecha</th>
    </tr>
    </thead>
    <tbody>
    <?php if(!is_null($conversacion)) :?>
    <?php foreach ($conversacion as $msg) : ?>

    <tr>
        <?php if($msg->getRemitente() == $_usuario->getId()) :?>
            <td class="btn-primary col-md-1"><?= ($usuarioRepository->find($msg->getRemitente()))->getUsername()?></td>
        <?php else:?>
            <td class="btn-warning col-md-1"><?= ($usuarioRepository->find($msg->getRemitente()))->getUsername()?></td>
        <?php endif;?>
        <td class="col-md-8"><?= $msg->getMensaje();?></td>
        <td class="col-md-2"><?= $msg->getFechaFormateada();?></td>
    </tr>
    <?php endforeach; ?>
    <?php endif;?>
    </tbody>
</table>


<form action="/mensajes/enviar/<?= $remitenteId?>" method="post">
    <textarea rows="7" class="col-md-9" name="mensaje" placeholder="Tu mensaje..."></textarea>
    <input type="submit" class="btn btn-primary pull-right col-md-2" value="Enviar">
</form>