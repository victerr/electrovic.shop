
<div class="container">
    <?php $i =0; foreach ($articulos as $articulo)  : ?>
        <?php $i+=1;?>
        <e>
            <div class="card col-md-4 text-center" >
                <img src="/articulos/<?= $articulo->getId() ?>/foto" class="card-img-top" height="250px" alt="/articulos/<?= $articulo->getId() ?>/foto">
                <div class="card-body">
                    <h4 class="card-title"><?= $articulo->getNombre() ?></h4>
                    <p class="card-text"><?= $articulo->getDescripcion() ?></p>
                    <div class="row">
                        <div class="col-md-3">
                            <h6>Precio:</h6><?= $articulo->getPrecio() ?> €
                        </div>
                        <div class="col-md-5">
                            <h6>Categoria:</h6><?= $articulo->getCategoria() ?>
                        </div>
                        <div class="col-md-4">
                            <h6>Estado:</h6><?= $articulo->getEstado() ?>
                        </div>
                    </div><br>
                    <div class="row">
                        <a href="/articulos/<?= $articulo->getId() ?>" class="btn btn-primary">Ver</a>
                        <?php if ($_usuario) :?>
                        <a href="/usuarios/carro/<?= $i ?>" class="btn btn-danger"><i class="patata">Eliminar</i></a>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </e>
    <?php endforeach; ?>
    <div class="col-md-3 pull-right">
        <h1>Total:  <?php echo $total ?>€</h1>
    </div>
</div>
<script src="/js/carro.js"></script>                                                            