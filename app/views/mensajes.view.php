<br>
<table class="table table-hover">
    <thead class="thead-dark">
    <tr>
        <th scope="col"></th>
        <th scope="col">Destinatario</th>
        <th scope="col">Último mensaje</th>
        <th scope="col">Fecha último mensaje</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($conversaciones as $conversacion) :?>
        <?php if(!empty($conversacion)): ?>
            <tr>
                <td class="col-md-1"><a href="/mensajes/conversacion/<?= $conversacion->getDestinatario()?>" class="btn btn-secondary col-md-2"><i class="fa fa-book-open"></i></a></td>
                <td class="col-md-1"><?=($usuarioRepositorio->find($conversacion->getDestinatario()))->getUsername();?></td>
                <td class="col-md-8"><?=$conversacion->getMensaje()?></td>
                <td class="col-md-2"><?= $conversacion->getFechaFormateada()?></td>
            </tr>
        <?php endif; ?>
    <?php endforeach; ?>
    </tbody>
</table>