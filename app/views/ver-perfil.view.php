<div class="jumbotron alert-info">
    <img src="/usuarios/<?= $usuario->getId() ?>/foto" class="col-md-3">
    <div class="container">
        <h2><?= $usuario->getUsername() ?></h2>
        <h5><?= $usuario->getEmail() ?> </h5>
        <strong>Fecha de registro:</strong> <?= $usuario->getFechaRegistro() ?><br>
        <strong>Artículos publicados: </strong><br>
        <strong>Rango: </strong> <?= $usuario->getRango() ?><br>
        <br><br><br><br><br>

        <hr class="alert-danger">
        <?php if($_usuario->getId() == $usuario->getId()) : ?>
        <h3>Modificar datos</h3>
        <form action="/usuarios/<?= $_usuario->getId()  ?>/actualizar" method="post" enctype="multipart/form-data">
            <div class="form-group col-md-4">
                <label for="password">Contraseña: </label>
                <input type="password" class="form-control col-md-3" name="password">
            </div>
            <div class="form-group col-md-4">
                <label for="password2">Repite la contraseña: </label>
                <input type="password" class="form-control col-md-3" name="password2">
            </div>
            <div class="form-group col-md-5">
                <label for="avatar">Modificar avatar:</label>
                <input type="file" class="form-control-file" name="avatar">
            </div>
            <div class="form-group col-md-4"><br>
                <label for="avatar">Idioma</label>
                <select class="custom-select">
                    <option selected>Open this select menu</option>
                    <option value="1">One</option>
                    <option value="2">Two</option>
                    <option value="3">Three</option>
                </select>
            </div>
            <button type="submit" class="btn btn-primary col-md-9 pull-right">Guardar cambios</button>
        </form>
        <?php else:?>

        <a class="btn btn-success" href="/mensajes/conversacion/<?=$usuario->getId()?>">Enviar un mensaje</a>
        <?php endif;?>
    </div>
</div>