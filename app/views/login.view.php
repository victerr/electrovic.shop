<br>
<form id="form-login" action="/check-login" method="post">
    <div class="form-group">
        <label for="login">Email/Usuario</label>
        <input type="text" class="form-control" name="login" id="login">
        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
    </div>
    <div class="form-group">
        <label for="password">Contraseña</label>
        <input type="password" class="form-control" name="password" id="password">
    </div>
    <button type="submit" class="btn btn-primary">Entrar</button>
</form>