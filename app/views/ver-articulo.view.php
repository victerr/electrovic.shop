<br><br><br>

<?php if (isset($id) && $id == $articulo->getId()) :?>
    <form action="/articulos/<?= $id ?>/actualizar" method="post">
        <div class="jumbotron text-center alert-info">
            <div class="container">
                <div class="col-md-8 col-md-offset-2">
                    <img src="/articulos/<?= $articulo->getId() ?>/foto" width="500px">
                    <hr>
                    <h2><input type="text" name="nombre" value="<?= $articulo->getNombre() ?>"></h2>
                    <h5><textarea class="form-control" name="descripcion" rows="3"><?= $articulo->getDescripcion() ?></textarea></h5>
                    <h4><input type="text" name="precio" value="<?= $articulo->getPrecio() ?>">€</h4>
                    <h6>Estado: <input type="text" name="estado" value="<?= $articulo->getEstado() ?>"></h6>
                    <h6>Categoría:
                        <select name="categoria">
                            <?php foreach ($categorias as $categoria) : ?>
                                <option value="<?= $categoria ?>"><?= $categoria ?></option>
                            <?php endforeach; ?>
                        </select></h6>

                    <input type="submit" class="btn btn-primary btn-lg" role="button" value="Guardar">
                    <a class="btn btn-primary btn-lg" href="/articulos/<?= $id ?>" type="submit" role="button">Cancelar</a>

                </div>
            </div>
        </div>
    </form>

<?php else: ?>
<div class="jumbotron text-center alert-info">
    <div class="container">
        <div class="col-md-8 col-md-offset-2">
            <img src="/articulos/<?= $articulo->getId() ?>/foto" width="500px">
            <hr>
            <h2><?= $articulo->getNombre() ?></h2>
            <h5><?= $articulo->getDescripcion() ?></h5>
            <h4><?= $articulo->getPrecio() ?>€</h4>
            <h6>Estado: <?= $articulo->getEstado() ?></h6>
            <?php if ($_usuario) :?>
                <?php if ($articulo->getUsuario() != $_usuario->getId()) :?>
                    <a class="btn btn-primary btn-lg" href="/articulos/<?= $articulo->getId() ?>/carro" role="button">Añadir al carro</a>
                <?php endif; ?>
                <?php if ($articulo->getUsuario() == $_usuario->getId() || $_usuario->getRole()=='ROLE_ADMIN') :?>
                    <a class="btn btn-primary btn-lg" href="/articulos/<?= $articulo->getId() ?>/editar" role="button">Modificar</a>
                    <a href="/articulos/<?= $articulo->getId() ?>" class="btn btn-danger btn-lg"><i class="patata">Eliminar</i></a>
                <?php endif; ?>
            <?php endif; ?>

            <?php endif; ?>
        </div>
    </div>
</div>
<script src="/js/articulos.js"></script>