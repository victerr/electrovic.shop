<?php

namespace EV\app\repository;

use EV\core\App;
use EV\core\database\QueryBuilder;
use EV\app\entity\Articulo;
use EV\core\exceptions\QueryException;

class ArticuloRepository extends QueryBuilder
{
    /**
     * @return ArticuloRepository
     */
    public static function getRepository() : ArticuloRepository
    {
        return App::getRepository(ArticuloRepository::class);
    }

    public function __construct()
    {
        parent::__construct('articulo', Articulo::class,  $loadDataBeforeConstruct=true);
    }

    /**
     * @param Articulo $articulo
     * @throws QueryException
     */
    public function nuevo(Articulo $articulo)
    {
            $this->save($articulo);
    }

    /**
     * @param Articulo $articulo
     * @throws QueryException
     */
    public function elimina(Articulo $articulo)
    {
        $this->remove($articulo);
    }
}