<?php


namespace EV\app\repository;


use EV\app\entity\Mensaje;
use EV\core\App;
use EV\core\database\QueryBuilder;

class Mensaje_RecibidoRepository extends  QueryBuilder
{
    /**
     * @return Mensaje_RecibidoRepository
     */
    public static function getRepository() : Mensaje_RecibidoRepository
    {
        return App::getRepository(Mensaje_RecibidoRepository::class);
    }

    public function __construct()
    {
        parent::__construct('mensaje_recibido', Mensaje::class,  $loadDataBeforeConstruct=true);
    }

    /**
     * @param Mensaje $mensaje
     * @throws \EV\core\exceptions\QueryException
     */
    public function nuevo(Mensaje $mensaje)
    {
        $fnTransaction = function () use ($mensaje)
        {
            $this->mensaje_enviado($mensaje);
            $this->mensaje_recibido($mensaje);
        };

        $this->executeTransaction($fnTransaction);
    }

    public function mensaje_enviado(Mensaje $mensaje_enviado)
    {
        $this->save($mensaje_enviado);
    }

    public function mensaje_recibido(Mensaje $mensaje_recibido)
    {
        $this->save($mensaje_recibido);
    }

    /**
     * @param Mensaje $mensaje
     * @throws \EV\core\exceptions\QueryException
     */
    public function elimina(Mensaje $mensaje)
    {
        $this->remove($mensaje);
    }
}