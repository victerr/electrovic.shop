<?php

namespace EV\app\repository;

use EV\app\entity\Usuario;
use EV\core\App;
use EV\core\database\QueryBuilder;

class UsuarioRepository extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct('usuario', Usuario::class,  $loadDataBeforeConstruct=false);
    }

    /**
     * @return UsuarioRepository
     */
    public static function getRepository() : UsuarioRepository
    {
        return App::getRepository(UsuarioRepository::class);
    }
}