<?php

namespace EV\app\entity;

interface IEntity
{
    public function toArray();
}