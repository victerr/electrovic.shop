<?php

namespace EV\app\entity;

use DateTime;

class Articulo implements IEntity
{
    /**
     * @var integer
     */
    private $id;
    /**
     * @var string
     */
    private $nombre;
    /**
     * @var string
     */
    private $descripcion;
    /**
     * @var int
     */
    private $precio;
    /**
     * @var string
     */
    private $categoria;
    /**
    * @var string
    */
    private $estado;
    /**
     * @var string
     */
    private $foto;
    /**
     * @var DateTime
     */
    private $fecha;
    /**
     * @var int
     */
    private $usuario;

    /**
     * Articulo constructor.
     */
    public function __construct()
    {
        if (is_null($this->fecha))
            $this->fecha = new DateTime();
        else
            $this->fecha = DateTime::createFromFormat('Y-m-d H:i:s', $this->fecha);
    }

    public function __toString()
    {
        return 'ID:  ' . $this->id .
            ' NOMBRE: ' . $this->nombre .
            ' DESCRIPCIÓN: ' . $this->descripcion.
            ' PRECIO: ' . $this->precio .
            ' CATEGORIA: ' . $this->categoria.
            ' ESTADO: ' . $this->estado;
    }

    public function __clone()
    {
        $this->fecha = clone $this->fecha;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Articulo
     */
    public function setId(int $id): Articulo
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getNombre(): string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     * @return Articulo
     */
    public function setNombre(string $nombre): Articulo
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescripcion(): string
    {
        return $this->descripcion;
    }

    /**
     * @param string $descripcion
     * @return Articulo
     */
    public function setDescripcion(string $descripcion): Articulo
    {
        $this->descripcion = $descripcion;
        return $this;
    }

    /**
     * @return int
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * @param int $precio
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;
    }

    /**
     * @return string
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * @param string $categoria
     */
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;
    }

    /**
     * @return string
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param string $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }


    /**
     * @return string
     */
    public function getFoto(): string
    {
        return $this->foto;
    }

    /**
     * @param string $foto
     * @return Articulo
     */
    public function setFoto(string $foto): Articulo
    {
        $this->foto = $foto;
        return $this;
    }

    public function getMiniatura()
    {
        $filePath = $this->foto;
        $posBarra = strripos($filePath, '/');
        $miniaturaPath = substr($filePath, 0, $posBarra);
        $miniaturaPath .= '/articulos/miniaturas';
        $miniaturaPath .= substr($filePath, $posBarra);
        return $miniaturaPath;
    }

    /**
     * @return DateTime
     */
    public function getFecha() : DateTime
    {
        return $this->fecha;
    }

    public function getFechaFormateada() : string
    {
        return $this->fecha->format('d/m/Y H:i:s');
    }

    /**
     * @param DateTime $fecha
     * @return Articulo
     */
    public function setFecha(DateTime $fecha): Articulo
    {
        $this->fecha = $fecha;
        return $this;
    }
    
    /**
     * @return int
     */
    public function getUsuario(): int
    {
        return $this->usuario;
    }

    /**
     * @param int $usuario
     * @return Articulo
     */
    public function setUsuario(int $usuario): Articulo
    {
        $this->usuario = $usuario;
        return $this;
    }

    public function toArray()
    {
        return [
            'id' => $this->id,
            'nombre' => $this->nombre,
            'descripcion' => $this->descripcion,
            'precio' => $this->precio,
            'categoria' => $this->categoria,
            'estado' => $this->estado,
            'foto' => $this->foto,
            'usuario' =>$this->usuario
        ];
    }
}