<?php

namespace EV\app\entity;

use DateTime;

class Usuario implements IEntity
{
    /**
     * @var integer
     */
    private $id;
    /**
     * @var string
     */
    private $username;
    /**
     * @var string
     */
    private $email;
    /**
     * @var string
     */
    private $password;
    /**
     * @var string
     */
    private $avatar;
    /**
     * @var string
     */
    private $rango;
    /**
     * @var DateTime
     */
    private $fecha_registro;
    /**
     * @var string
     */
    private $role;

    public function __construct()
    {
        if (is_null($this->fecha_registro))
            $this->fecha_registro = new DateTime();
        else
            $this->fecha_registro = DateTime::createFromFormat('Y-m-d H:i:s', $this->fecha_registro);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return Usuario
     */
    public function setUsername(string $username): Usuario
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return Usuario
     */
    public function setPassword(string $password): Usuario
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return string
     */
    public function getAvatar(): string
    {
        return $this->avatar;
    }

    /**
     * @param string $avatar
     */
    public function setAvatar(string $avatar)
    {
        $this->avatar = $avatar;
    }

    /**
     * @return string
     */
    public function getRango()
    {
        return $this->rango;
    }

    /**
     * @param string $rango
     */
    public function setRango($rango)
    {
        $this->rango = $rango;
    }

    /**
     * @return DateTime
     */
    public function getFechaRegistro()
    {
        return $this->fecha_registro;
    }

    /**
     * @param DateTime $fecha_registro
     */
    public function setFechaRegistro($fecha_registro)
    {
        $this->fecha_registro = $fecha_registro;
    }

    public function getFechaRegistroFormateada() : string
    {
        return $this->fecha_registro->format('d/m/Y H:i:s');
    }
    /**
     * @return string
     */
    public function getRole(): string
    {
        return $this->role;
    }

    /**
     * @param string $role
     * @return Usuario
     */
    public function setRole(string $role): Usuario
    {
        $this->role = $role;
        return $this;
    }

    public function toArray()
    {
        return [
            'id' => $this->id,
            'username' => $this->username,
            'email' => $this->email,
            'password' => $this->password,
            'avatar' => $this->avatar,/*
            'fecha_nac' => $this->fecha_nac,*/
            'rango' => $this->rango,/*
            'fecha_registro' => $this->fecha_registro,*/
            'role' => $this->role
        ];
    }
}