<?php


namespace EV\app\entity;


use Couchbase\DateRangeSearchFacet;
use DateTime;

class Mensaje implements \EV\app\entity\IEntity
{
    /**
     * @var integer
     */
    private $id;
    /**
     * @var int
     */
    private $remitente;
    /**
     * @var int
     */
    private $destinatario;
    /**
     * @var string
     */
    private $mensaje;
    /**
     * @var DateTime
     */
    private $fecha;


    public function __construct()
    {
        if (is_null($this->fecha))
            $this->fecha = new DateTime();
        else
            $this->fecha = DateTime::createFromFormat('Y-m-d H:i:s', $this->fecha);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getRemitente()
    {
        return $this->remitente;
    }

    /**
     * @param int $remitente
     */
    public function setRemitente($remitente)
    {
        $this->remitente = $remitente;
    }

    /**
     * @return int
     */
    public function getDestinatario()
    {
        return $this->destinatario;
    }

    /**
     * @param int $destinatario
     */
    public function setDestinatario($destinatario)
    {
        $this->destinatario = $destinatario;
    }

    /**
     * @return string
     */
    public function getMensaje()
    {
        return $this->mensaje;
    }

    /**
     * @param string $mensaje
     */
    public function setMensaje($mensaje)
    {
        $this->mensaje = $mensaje;
    }

    /**
     * @return DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * @param DateTime $fecha
     */
    public function setFecha(DateTime $fecha): Mensaje
    {
        $this->fecha = $fecha;
        return $this;
    }

    public function getFechaFormateada() : string
    {
        return $this->fecha->format('d/m/Y H:i:s');
    }

    public function toArray()
    {
        return [
            'id' => $this->id,
            'remitente' => $this->remitente,
            'destinatario' => $this->destinatario,
            'mensaje' => $this->mensaje
        ];
    }


}