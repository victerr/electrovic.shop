<?php

namespace EV\core;

class Security
{
    /**
     * @param string $role
     * @return bool
     * @throws exceptions\AppException
     */
    public static function isUserGranted(string $role) : bool
    {
        if ($role === 'ROLE_ANONYMOUS')
            return true;

        if (!self::isLogged())
            return false;

        $roles = App::get('config')['security']['roles'];
        if ($roles[App::get('usuario')->getRole()] >= $roles[$role])
            return true;

        return false;
    }

    /**
     * @return bool
     * @throws exceptions\AppException
     */
    public static function isLogged() : bool
    {
        if (App::get('usuario') !== null)
            return true;

        return false;
    }

    public static function encrypt(string $password) : string
    {
        return password_hash($password, PASSWORD_BCRYPT);
    }

    public static function checkPassword(
        string $password, string $bdPassword)
    {
        return password_verify($password, $bdPassword);
    }
}