<?php

namespace EV\core\database;

use EV\core\App;
use EV\core\exceptions\QueryException;
use PDO;
use PDOException;

class Connection
{
    /**
     * @return PDO
     * @throws AppException
     * @throws QueryException
     */
    public static function make() : PDO
    {
        try
        {
            $config = App::get('config')['database'];

            $pdo = new PDO(
                $config['connection'] . ';dbname=' . $config['dbname'],
                $config['dbuser'],
                $config['dbpassword'],
                $config['options']
            );
        }
        catch (PDOException $pdoException)
        {
            throw new QueryException("No se ha podido conectar a la BBDD.", $pdoException->getMessage());
        }

        return $pdo;
    }
}