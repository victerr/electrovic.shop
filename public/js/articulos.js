document.addEventListener('DOMContentLoaded', function () {
    let trashes = [...document.getElementsByClassName('patata')];

    trashes.forEach(trash  => {
        trash.addEventListener('click', function (event) {
            event.preventDefault();

            let link = event.target.parentElement;
            let url = link.href;

            fetch(url, {method: 'DELETE'}).then(response => {
                if (response.ok === true) {
                    link.closest('e').remove();
                    window.location('/articulos');
                }
            } );
        });
    });
});